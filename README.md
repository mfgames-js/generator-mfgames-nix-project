# Yeoman Generator for MfGames Nix Projects

A generator for creating an opinionated project layout which is useful for both code and writing. It is based around using a [Nix](https://nixos.org/) flakes for reproducible builds and adding hooks for [direnv](https://direnv.net/) for automatic wiring with that flake.

## Package Management

This treats a `package.json` file as a generic package metadata. This means it will use the `scripts` tags to create hooks for build, test, and release tasks. When using continual integration or deployment systems (CI/CD) such as Gitlab, it will also create scripts in the `scripts` folder to run those and set up the appropriate pipelines to use Nix flakes.

For Docker images, this uses the [nix-flake-docker](https://gitlab.com/dmoonfire/nix-flake-docker/) image to set up a Nix environment for building.

## Release Management

Much of the process is centered around [Semantic Release](https://semantic-release.gitbook.io/semantic-release/) for non-romantic versioning and releases. When including a release process, that will be automatically configured to work for Gitlab and [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) can be used to control the versioning process.

## Git Management

During the build process, this will offer to create a Git repository and set the initial tag, including creating a `vX.X.X` tag which can be pushed up to the build process to control the semantic release.
