const fs = require("fs");
const nix = require("../../lib/index");

function prompt(generator) {
    return [];
}

async function configure(generator) {
    generator.fs.copyTpl(
        generator.templatePath("flake.nix"),
        generator.destinationPath("flake.nix"),
        {
            description: generator.answers.description,
            packages: nix.getNixPackages(),
        }
    );

    generator.fs.copyTpl(
        generator.templatePath("envrc"),
        generator.destinationPath(".envrc"),
        {}
    );
}

async function install(generator) {}

async function end(generator) {
    generator.log("Adding Nix flake");
    await generator._spawnCommand("git", ["add", "flake.nix"]);

    generator.log("Building Nix lock");
    await generator._spawnCommand("nix", ["flake", "lock"]);

    generator.log("Adding Nix lock");
    await generator._spawnCommand("git", ["add", "flake.lock"]);

    generator.log("Trusting .envrc");
    await generator._spawnCommand("direnv", ["allow"]);
}

module.exports = {
    prompt,
    configure,
    install,
    end,
};
