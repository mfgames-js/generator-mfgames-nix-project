const _ = require("lodash");
const Generator = require("yeoman-generator");
const build = require("./build");
const git = require("./git");
const gitlab = require("./gitlab");
const metadata = require("./metadata");
const nix = require("./nix");

// Yeoman 5.x.x requires we extend the generator to include the deprecated
// install actions.
_.extend(Generator.prototype, require("yeoman-generator/lib/actions/install"));

// Create the class that represents our application.
module.exports = class extends Generator {
    constructor(args, opts) {
        super(args, opts);

        build.setup(this);
        gitlab.setup(this);

        // Option to skip executing any program, used for tests.
        this.option("skipSpawn", { type: Boolean, default: false });
    }

    async prompting() {
        this.answers = await this.prompt(
            [].concat(
                metadata.prompt(this),
                git.prompt(this),
                build.prompt(this),
                gitlab.prompt(this),
                nix.prompt(this)
            )
        );
    }

    async configuring() {
        // Run the rest of the configurations.
        await metadata.configure(this);
        await git.configure(this);
        await build.configure(this);
        await gitlab.configure(this);
        await nix.configure(this);
    }

    async writing() {
        // Create the template files.
        this.fs.copyTpl(
            this.templatePath("gitignore.txt"),
            this.destinationPath(".gitignore"),
            {}
        );

        // Write out the .yo-rc.json.
        this.config.save();
    }

    async installing() {
        await git.install(this);
        await build.install(this);
        await gitlab.install(this);
        await nix.install(this);

        this.installDependencies();
    }

    async end() {
        await build.end(this);
        await nix.end(this);
        await git.end(this);
    }

    async _spawnCommand(name, args) {
        if (this.options.skipSpawn) {
            this.log("spawn:", name, args);
            return Promise.resolve();
        }

        await this.spawnCommand(name, args);
    }

    get(name, defaultValue) {
        const answer = this.answers[name];
        const option = this.options[name];

        if (answer !== undefined) {
            return answer;
        }

        if (option !== undefined) {
            return option;
        }

        return defaultValue;
    }
};
