<% if (packageManager === "yarn") { %>
npm install -g yarn
yarn install --frozen-lockfile
<% } else { %>
npm install --ci
<% } %>

<%= packageManager %> run build
<%= packageManager %> run test
