<% if (packageManager === "yarn") { %>
npm install -g yarn
yarn install --frozen-lockfile
<% } else { %>
npm install --ci
<% } %>

# `<%= packageManager %> run build` will be called in the release process
# otherwise, it won't pick up changes made to the bumped version in
# `package.json`.
npx semantic-release
