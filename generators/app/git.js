const fs = require("fs");

function prompt(generator) {
    return [
        {
            when: (_) => !fs.existsSync(".git"),
            type: "confirm",
            name: "initGit",
            message: "Do you want to create the Git repository?",
            store: true,
        },
        {
            when: (answers) => answers.initGit,
            type: "input",
            name: "postInitGit",
            message: "Any command to run after `git init`?",
            store: true,
        },
        {
            when: (answers) => answers.initGit,
            type: "confirm",
            name: "commitGit",
            message: "Do you want to create the initial commit?",
            store: true,
        },
        {
            when: (answers) => answers.commitGit,
            type: "confirm",
            name: "initialTag",
            message: "Do you want to create the initial Git tag?",
            default: true,
            store: true,
        },
    ];
}

async function configure(generator) {}

async function install(generator) {
    if (generator.answers.initGit) {
        generator.log("Creating initial Git repository");
        await generator._spawnCommand("git", ["init", "--initial-branch=main"]);
    }

    if (generator.answers.postInitGit) {
        generator.log("Running custom Git command");
        await generator._spawnCommand(generator.answers.postInitGit);
    }
}

async function end(generator) {
    if (generator.answers.initGit) {
        generator.log("Adding all files to Git");
        await generator._spawnCommand("git", ["add", "-A"]);
    }

    if (generator.answers.commitGit) {
        generator.log("Running initial Git commit");
        await generator._spawnCommand("git", [
            "commit",
            "-am",
            generator.answers.conventionalCommits
                ? "chore: initial commit"
                : "Initial commit",
        ]);
    }

    if (generator.answers.initialTag) {
        generator.log("Tagging initial Git commit");
        await generator._spawnCommand("git", [
            "tag",
            `v${generator.answers.version}`,
        ]);
    }

    // Make some noise about the process.
    generator.log(
        "Remember to call `git push --tags` before pushing up the branch so any CI/CD processes pick it up"
    );
}

module.exports = {
    prompt,
    configure,
    install,
    end,
};
