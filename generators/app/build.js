const sortPackageJson = require("sort-package-json");
const fs = require("fs");
const project = require("../../lib/index");

function setup(generator) {
    generator.option("buildCommand", {
        type: String,
        default: "echo build successful",
    });
    generator.option("hasTest", { type: Boolean });
    generator.option("testCommand", { type: String });
    generator.option("hasRelease", { type: Boolean });
    generator.option("packageSortOrder");
}

function prompt(generator) {
    return [
        {
            type: "list",
            name: "packageManager",
            message: "Which package manager do you use?",
            choices: [
                { name: "NPM", value: "npm" },
                { name: "Yarn", value: "yarn" },
            ],
            store: true,
        },
        {
            when: () => generator.options.buildCommand === undefined,
            type: "input",
            name: "buildCommand",
            message: "What command to run to build:",
            default: "echo build successful",
        },
        {
            type: "input",
            name: "usePrettier",
            message: "Do you want to use Prettier?",
            default: true,
        },
        {
            when: () => generator.options.hasTest === undefined,
            type: "confirm",
            name: "hasTest",
            message: "Do you want want tests?",
            default: false,
        },
        {
            when: (answers) => answers.hasTest,
            type: "confirm",
            name: "hasCustomTest",
            message: "Do you want to have a test command?",
            default: true,
        },
        {
            when: (answers) => answers.hasCustomTest,
            type: "input",
            name: "testCommand",
            message: "What command to run to test:",
            default: "echo test successful",
        },
        {
            when: () => generator.options.hasRelease === undefined,
            type: "confirm",
            name: "hasRelease",
            message: "Do you want to have a release process?",
            default: false,
        },
        {
            when: (answers) =>
                answers.hasRelease || generator.options.hasRelease,
            type: "confirm",
            name: "semanticReleases",
            message: "Do you want to use semantic releases?",
            default: true,
            store: true,
        },
        {
            type: "confirm",
            name: "conventionalCommits",
            message: "Do you want to use conventional commits?",
            default: true,
            store: true,
        },
        {
            when: (answers) => answers.conventionalCommits,
            type: "confirm",
            name: "husky",
            message: "Do you want to use Husky?",
            default: true,
            store: true,
        },
        {
            type: "confirm",
            name: "ci",
            message: "Do you want to use CI/CD?",
            default: true,
            store: true,
        },
    ];
}

async function configure(generator) {
    // Yeoman uses an environment variable to let us choose the right package
    // manager. We use this to avoid an error message about unknown package
    // manager.
    generator.env.options.nodePackageManager = generator.answers.packageManager;

    // Update the package tool.
    const pkgJson = {
        scripts: {
            build: generator.get("buildCommand"),
        },
    };

    if (generator.answers.usePrettier) {
        pkgJson.scripts.format = "prettier . --write --loglevel warn";
        pkgJson.scripts.prebuild =
            generator.answers.packageManager + " run format";

        generator.fs.copyTpl(
            generator.templatePath("prettierignore"),
            generator.destinationPath(".prettierignore")
        );
    }

    if (generator.get("hasTest")) {
        const testCommand = generator.get("testCommand");
        pkgJson.scripts["test"] = "run-s test:*";

        if (testCommand !== undefined) {
            pkgJson.scripts["test:run"] = testCommand;
        }
    }

    // If we are using semantic releases, then we need to include the
    // configuration.
    if (generator.answers.semanticReleases) {
        generator.fs.copyTpl(
            generator.templatePath("release.config.js"),
            generator.destinationPath("release.config.js"),
            {
                packageManager: generator.answers.packageManager,
                conventionalCommits: generator.answers.conventionalCommits,
                gitlab: generator.answers.gitlab,
                gitlabAssets: project.getGitlabAssets(),
            }
        );
    }

    // commitlint has a configuration.
    if (generator.answers.conventionalCommits) {
        if (generator.get("hasTest")) {
            pkgJson.scripts["test:commits"] =
                "commitlint-gitlab-ci -x @commitlint/config-conventional";
        }

        generator.fs.copyTpl(
            generator.templatePath("commitlint.config.js"),
            generator.destinationPath("commitlint.config.js"),
            {
                packageManager: generator.answers.packageManager,
            }
        );
    }

    // Husky has the commit hook that we also call in the scripts.
    if (generator.answers.husky) {
        generator.fs.extendJSON(generator.destinationPath("package.json"), {
            scripts: {
                prepare: "husky install",
            },
        });

        generator.fs.copyTpl(
            generator.templatePath("commit-msg.husky"),
            generator.destinationPath(".husky/commit-msg"),
            {
                packageManager: generator.answers.packageManager,
            }
        );
    }

    // Write out the package to the file system.
    generator.fs.extendJSON(generator.destinationPath("package.json"), pkgJson);
}

async function install(generator) {
    // If we have a test, then we use run-all.
    if (generator.get("hasTest")) {
        generator.addDevDependencies({
            "npm-run-all": "^4.1.5",
        });
    }

    // See if we are using Prettier.
    if (generator.answers.usePrettier) {
        generator.addDevDependencies({
            prettier: "^2.0.5",
        });
    }

    // If we are doing semantic releases, we need to add a number of packages.
    if (generator.answers.semanticReleases) {
        generator.addDevDependencies({
            "@semantic-release/changelog": "^6.0.1",
            "@semantic-release/exec": "^6.0.3",
            "@semantic-release/release-notes-generator": "^10.0.3",
            "@semantic-release/npm": "^8.0.3",
            "@semantic-release/git": "^10.0.1",
            "semantic-release": "^18.0.1",
        });
    }

    // commitlint has packages.
    if (generator.answers.conventionalCommits) {
        generator.addDevDependencies({
            "@commitlint/cli": "^15.0.0",
            "@commitlint/config-conventional": "^15.0.0",
        });
    }

    // Husky also has packages.
    if (generator.answers.husky) {
        generator.addDevDependencies({
            husky: "^7.0.2",
        });
    }
}

async function end(generator) {
    // When we are using Yarn as the package manager, this generator also
    // creates a `package-lock.json` file that we don't have. To get around
    // this, we hack out a shell script to remove it.
    if (generator.answers.packageManager === "yarn") {
        generator.log("Removing NPM lock file because Yarn is in use");

        await generator._spawnCommand("rm", ["package-lock.json"]);
    }

    // Sort the contents of the `package.json` file to make it prettier.
    generator.log("Cleaning up package.json");

    const pkgJson = fs.readFileSync("package.json").toString();
    const sortedJson = sortPackageJson(pkgJson);

    fs.writeFileSync("package.json", sortedJson);

    // If we have Prettier, then run it to clean up the files.
    if (generator.answers.usePrettier) {
        generator.log("Cleaning up files using Prettier");
        await generator._spawnCommand(generator.answers.packageManager, [
            "run",
            "format",
        ]);
    }
}

module.exports = {
    setup,
    prompt,
    configure,
    install,
    end,
};
