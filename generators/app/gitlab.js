function setup(generator) {
    generator.option("gitlab", { type: Boolean, default: true });
    generator.option("gitlabSast", { type: Boolean, default: true });
}

function prompt(generator) {
    return [
        {
            type: "confirm",
            name: "gitlab",
            message: "Do you use Gitlab for your CI/CD?",
            default: true,
            store: true,
        },
        {
            when: (answers) =>
                answers.hasTest && generator.options.gitlabSast === undefined,
            type: "confirm",
            name: "gitlabSast",
            message: "Do you use want to use Gitlab's SAST testing?",
            default: true,
        },
    ];
}

async function configure(generator) {
    // If we aren't doing Gitlab, then don't do anything.
    if (!generator.answers.gitlab) {
        return;
    }

    // Create the templates.
    generator.fs.copyTpl(
        generator.templatePath("gitlab-ci.yml"),
        generator.destinationPath(".gitlab-ci.yml"),
        {
            gitlabSast: generator.get("gitlabSast"),
            hasTest: generator.get("hasTest", false),
            hasRelease: generator.get("hasRelease", false),
        }
    );

    generator.fs.copyTpl(
        generator.templatePath("ci-build.sh"),
        generator.destinationPath("scripts/ci-build.sh"),
        generator.answers
    );

    if (generator.get("hasTest")) {
        generator.fs.copyTpl(
            generator.templatePath("ci-test.sh"),
            generator.destinationPath("scripts/ci-test.sh"),
            generator.answers
        );
    }

    if (generator.get("hasRelease")) {
        generator.fs.copyTpl(
            generator.templatePath("ci-release.sh"),
            generator.destinationPath("scripts/ci-release.sh"),
            generator.answers
        );
    }
}

async function install(generator) {
    // If we aren't doing Gitlab, then don't do anything.
    if (!generator.answers.gitlab) {
        return;
    }

    // If we have conventional commits, add Gitlab-specific packages.
    if (generator.answers.conventionalCommits) {
        generator.addDevDependencies({
            "commitlint-gitlab-ci": "^0.0.4",
        });
    }

    // If we are doing semantic releases, we need to add a number of packages.
    if (generator.answers.semanticReleases) {
        generator.addDevDependencies({
            "@semantic-release/gitlab": "^7.0.4",
        });
    }
}

module.exports = {
    setup,
    prompt,
    configure,
    install,
};
