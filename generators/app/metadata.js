const slugify = require("slugify");

function prompt(generator) {
    return [
        {
            type: "input",
            name: "slug",
            message: "The project name or slug:",
            default: (answers) => slugify(generator.appname, { lower: true }),
        },
        {
            type: "input",
            name: "authorName",
            message: "The author's name:",
        },
        {
            type: "input",
            name: "authorEmail",
            message: "The author's email:",
        },
        {
            type: "input",
            name: "authorUrl",
            message: "The author's URL:",
        },
        {
            type: "input",
            name: "version",
            message: "What version do you want to start?",
            default: "0.0.1",
        },
        {
            type: "input",
            name: "description",
            message: "Describe the package?",
        },
    ];
}

async function configure(generator) {
    // Figure out the package file.
    let pkgJson = {
        name: generator.answers.slug,
        private: true,
        version: generator.answers.version,
    };

    if (generator.answers.description && generator.answers.description !== "") {
        pkgJson.description = generator.answers.description;
    }

    // Include the author information, if provided.
    const hasAuthorName =
        generator.answers.authorName && generator.answers.authorName !== "";
    const hasAuthorEmail =
        generator.answers.authorEmail && generator.answers.authorEmail !== "";
    const hasAuthorUrl =
        generator.answers.authorUrl && generator.answers.authorUrl !== "";

    if (hasAuthorName || hasAuthorEmail || hasAuthorUrl) {
        const author = {};

        if (hasAuthorName) {
            author.name = generator.answers.authorName;
        }
        if (hasAuthorEmail) {
            author.email = generator.answers.authorEmail;
        }
        if (hasAuthorUrl) {
            author.url = generator.answers.authorUrl;
        }

        pkgJson.author = author;
    }

    // Write out the package to the file system.
    generator.fs.extendJSON(generator.destinationPath("package.json"), pkgJson);

    // EditorConfig is awesome.
    generator.fs.copyTpl(
        generator.templatePath("editorconfig"),
        generator.destinationPath(".editorconfig")
    );
}

module.exports = {
    prompt,
    configure,
};
