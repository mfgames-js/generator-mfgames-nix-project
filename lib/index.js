let packages = {
    "pkgs.nodejs-16_x": { name: "Node 16.x" },
    "pkgs.nixfmt": {},
};

let gitlabAssets = [];

/**
 * Adds an asset for the Gitlab release. This follows the conventions from
 * https://github.com/semantic-release/gitlab.
 *
 * @param {string} The relative path to add. Can be a globbed path.
 * @param {string?} The optional label for the asset.
 * @param {string?} The optional type, defaults to "other". Can be package, image, runbook, or other.
 */
function addGitlabAsset(path, label, type) {
    gitlabAssets.push({ path: path, label: label, type: type || "other" });
}

function getGitlabAssets() {
    if (gitlabAssets.length === 0) {
        return "";
    }

    return JSON.stringify(gitlabAssets);
}

/**
 * Adds a Nix package with some additional data to describe it.
 *
 * name: The human-friendly name of the package.
 * url: The URL to the package.
 */
function addNixPackage(packageName, data) {
    packages[packageName] = data;
}

function getNixPackages() {
    let names = Object.keys(packages);
    names.sort();
    return names.join(" ");
}

module.exports = {
    addGitlabAsset: addGitlabAsset,
    getGitlabAssets: getGitlabAssets,
    addNixPackage: addNixPackage,
    getNixPackages: getNixPackages,
};
