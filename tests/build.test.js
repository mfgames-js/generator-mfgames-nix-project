const helpers = require("yeoman-test");
const assert = require("yeoman-assert");
const path = require("path");
const fs = require("fs");

const defaultAnswers = {
    authorName: "",
    authorEmail: "",
    authorUrl: "",
    description: "Describe the project",
    initGit: false,
    postInitGit: "",
    commitGit: false,
    packageManager: "npm",
    buildCommand: "echo xxx successful",
    hasTest: true,
    hasCustomTest: true,
    testCommand: "echo test successful",
    hasRelease: true,
    releaseCommand: "echo release successful",
};

describe("app", function () {
    describe("via answers", function () {
        it("generates a test command", function () {
            return helpers
                .run(path.join(__dirname, "../generators/app"))
                .withOptions({ skipSpawn: true })
                .withPrompts({
                    ...defaultAnswers,
                    hasTest: true,
                    hasCustomTest: true,
                    testCommand: "echo test successful",
                    hasRelease: false,
                })
                .then(function () {
                    assert.file([".gitignore", "scripts/ci-test.sh"]);
                    assert.noFile(["scripts/ci-release.sh"]);
                    assert.fileContent([
                        ["package.json", /echo test successful/],
                    ]);
                    assert.noFileContent([
                        ["package.json", /echo release successful/],
                    ]);
                });
        });

        it("generates a release command", function () {
            return helpers
                .run(path.join(__dirname, "../generators/app"))
                .withOptions({ skipSpawn: true })
                .withPrompts({
                    ...defaultAnswers,
                    hasTest: false,
                    hasRelease: true,
                })
                .then(function () {
                    assert.file([".gitignore", "scripts/ci-release.sh"]);
                    assert.noFile(["scripts/ci-test.sh"]);
                    assert.noFileContent([
                        ["package.json", /echo test successful/],
                    ]);
                });
        });
    });

    describe("via options", function () {
        it("generates a test command", function () {
            return helpers
                .run(path.join(__dirname, "../generators/app"))
                .withOptions({
                    skipSpawn: true,
                    hasTest: true,
                    testCommand: "echo custom test successful",
                })
                .withPrompts({
                    ...defaultAnswers,
                    hasTest: undefined,
                    testCommand: undefined,
                    hasRelease: undefined,
                })
                .then(function () {
                    assert.file([".gitignore", "scripts/ci-test.sh"]);
                    assert.noFile(["scripts/ci-release.sh"]);
                    assert.fileContent([
                        ["package.json", /echo custom test successful/],
                    ]);
                    assert.noFileContent([
                        ["package.json", /echo release successful/],
                    ]);
                });
        });

        it("generates a release command", function () {
            return helpers
                .run(path.join(__dirname, "../generators/app"))
                .withOptions({ skipSpawn: true, hasRelease: true })
                .withPrompts({
                    ...defaultAnswers,
                    hasTest: undefined,
                    hasRelease: undefined,
                })
                .then(function () {
                    assert.file([".gitignore", "scripts/ci-release.sh"]);
                    assert.noFile(["scripts/ci-test.sh"]);
                    assert.noFileContent([
                        ["package.json", /echo test successful/],
                    ]);
                });
        });
    });
});
