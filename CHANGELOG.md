## [2.0.1](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/compare/v2.0.0...v2.0.1) (2021-12-22)


### Bug Fixes

* include the index file for packaging ([618e7d1](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/618e7d1f140364d88acd4c76a971efeacd46778a))

# [2.0.0](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/compare/v1.5.0...v2.0.0) (2021-12-22)

# [1.5.0](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/compare/v1.4.2...v1.5.0) (2021-12-22)


### Bug Fixes

* if options.hasRelease, then ask release questions ([35d0c24](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/35d0c24a21244fb4268b9c7fcf03e8d1f3712165))
* **semantic-release:** build is run after the version is bumped ([2878e46](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/2878e460a9a857b8fd1da58ec590b4d5f8bb3642)), closes [#2](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/issues/2)
* updated how Nix packages are registered to get human-readable descriptions ([18f798e](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/18f798e706c87ff69aeef53144a3883e3dd46afa))


### Features

* add the ability to customize the Gitlab assets on release ([8fa132b](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/8fa132baacbff46bf3e0167df7eabd3a0cbcc40a)), closes [#5](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/issues/5)
* expose `addNixPackage` and `getNixPackages` as library functions ([5a2edac](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/5a2edac30b9b8e6da6c14fc643bc758d601597ac))

## [1.4.2](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/compare/v1.4.1...v1.4.2) (2021-12-22)


### Bug Fixes

* corrected the permissions on the test script ([9e1a96e](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/9e1a96ec805acce3f0a1610ea5727dcb286831f0))
* test and release commands show again ([f519197](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/f5191975edb12e6cc3390257cea365233a883238)), closes [#3](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/issues/3)

## [1.4.1](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/compare/v1.4.0...v1.4.1) (2021-12-20)


### Bug Fixes

* packaging sorting is not a dev dependency ([0ce8f94](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/0ce8f94bee2ce5f0479fc007361afa3f897ca9ae))

# [1.4.0](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/compare/v1.3.0...v1.4.0) (2021-12-20)


### Features

* sort the package.json file to make it prettier ([d56f060](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/d56f060987ed8083cf8e2c56e0d6d232df899663))

# [1.3.0](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/compare/v1.2.0...v1.3.0) (2021-12-20)


### Features

* changed the flow when composing with other generators ([39361b8](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/39361b869cc0756b5e320a038149653d337d0e2a))

# [1.2.0](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/compare/v1.1.0...v1.2.0) (2021-12-19)


### Features

* added Prettier to format files on build ([8cc11cd](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/8cc11cd879f907b4dd82551b46f58285bb531277))

# [1.1.0](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/compare/v1.0.1...v1.1.0) (2021-12-19)


### Bug Fixes

* cleaning up NPM/Yarn interaction ([ff61ff6](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/ff61ff6a94c267ab95244b8e755ef5c1e6e8bed6))
* generated Gitlab file calls the `script/ci-` versions correctly ([94e0276](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/94e0276dd2aadce5ce2137fb964f79b5d2cd3881))


### Features

* made test and release scripts optional, also Gitlab SAST testing ([401faa2](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/401faa2c8a458d5f865423a10d301280788c1b13))
* trust generated `.envrc` on build ([90ba99f](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/90ba99f9a86344bd7eaa930081ea8319f9bf2a06))

## [1.0.1](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/compare/v1.0.0...v1.0.1) (2021-12-12)


### Bug Fixes

* slugify the application name ([3f83c39](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/3f83c39a3e067a8688c45770469eca2e5ae0ee76))

# 1.0.0 (2021-12-11)


### Features

* initial commit ([17fae74](https://gitlab.com/mfgames-js/generator-mfgames-nix-project/commit/17fae749142797f5f49f5f0875dd8c7a25199ff1))
